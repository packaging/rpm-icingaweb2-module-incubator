# Incubator Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name incubator

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.3.0
Release:        %{revision}%{?dist}
Summary:        Icinga PHP library module for Icinga Web 2
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

%description
This module brings bleeding edge libraries useful for Icinga Web 2 modules.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%post
set -e

# Only for fresh installations
if [ $1 == 1 ]; then
    if [ ! -d /etc/icingaweb2/enabledModules ]; then
        mkdir /etc/icingaweb2/enabledModules
        chmod g=rwx,o= /etc/icingaweb2/enabledModules
    fi

    echo "Enabling icingaweb2 module '%{module_name}'"
    ln -svf /usr/share/icingaweb2/modules/%{module_name} /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md
#LICENSE

%defattr(-,root,root)
%{basedir}

%changelog
* Tue May 21 2019 Markus Frosch <markus.frosch@icinga.com> - 0.3.0-1
- Update to 0.3.0

* Thu May 02 2019 Markus Frosch <markus.frosch@icinga.com> - 0.2.0-1
- Initial package version
